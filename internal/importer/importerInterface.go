package importer

import (
	"context"
	"errors"

	serverPkg "gitlab.eclipse.org/eclipse/xfsc/libraries/microservice/core/pkg/server"
	"gitlab.eclipse.org/eclipse/xfsc/libraries/ssi/oid4vip/model/credential"
	"gitlab.eclipse.org/eclipse/xfsc/organisational-credential-manager-w-stack/well-known-service/internal/common"
)

var ErrNotFound = errors.New("not found")

type Importer interface {
	Start(ctx context.Context, server *serverPkg.Server, env *common.Environment) error
	Stop() error
	GotErrors() bool
	GetCredentialIssuerMetadata(ctx context.Context, tenantID string) (*credential.IssuerMetadata, error)
}
