package types

func BoolPtr(value bool) *bool {
	return &value
}
