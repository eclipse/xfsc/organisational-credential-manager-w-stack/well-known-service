package config

import (
	"time"

	"gitlab.eclipse.org/eclipse/xfsc/libraries/messaging/cloudeventprovider"
	cfgPkg "gitlab.eclipse.org/eclipse/xfsc/libraries/microservice/core/pkg/config"
	postgresPkg "gitlab.eclipse.org/eclipse/xfsc/libraries/microservice/core/pkg/db/postgres"
)

const (
	ImporterGit       = "GIT"
	ImporterBroadcast = "BROADCAST"
)

type Config struct {
	cfgPkg.BaseConfig `envconfig:"CORE"`

	Postgres         postgresPkg.Config            `envconfig:"POSTGRES"`
	Nats             cloudeventprovider.NatsConfig `envconfig:"NATS"`
	Git              GitConfig                     `envconfig:"GIT"`
	JwtIssuer        JwtIssuerConfig               `envconfig:"OPEN_ID"`
	CredentialIssuer CredentialIssuerConfig        `envconfig:"CREDENTIAL_ISSUER"`
	Gateway          GatewayConfig                 `envconfig:"GATEWAY"`
}

type GatewayConfig struct {
	LocationHeaderKey string `envconfig:"LOCATION_HEADER_KEY"`
	JwksUrlHeaderKey  string
}

type JwtIssuerConfig struct {
	Issuer string `envconfig:"ISSUER"`
}

type CredentialIssuerConfig struct {
	Importer string `envconfig:"IMPORTER" required:"true" default:"BROADCAST"`
}

type GitConfig struct {
	ImagePath string        `envconfig:"IMAGE_PATH"`
	Repo      string        `envconfig:"REPO"`
	Token     string        `envconfig:"TOKEN"`
	Interval  time.Duration `envconfig:"INTERVAL"`
}
