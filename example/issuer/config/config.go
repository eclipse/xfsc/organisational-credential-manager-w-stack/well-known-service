package config

import "gitlab.eclipse.org/eclipse/xfsc/libraries/messaging/cloudeventprovider"

type Config struct {
	Nats cloudeventprovider.NatsConfig `envconfig:"NATS"`
}
